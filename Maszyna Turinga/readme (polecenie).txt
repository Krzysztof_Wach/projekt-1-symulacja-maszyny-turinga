Opis:
Program powinien symulowa� dzia�anie Maszyny Turinga
oraz umo�liwi� prze�ledzenie kolejnych krok�w dzia�ania maszyny.
Dane wej�ciowe powinny by� pobierane z pliku sformatowane
wed�ug schematu. Program powinien mie� mo�liwo��
ustalenia alfabetu, liczby stan�w, s�owa, d�ugo�ci s�owa, okre�lenia
stan�w ko�cowych, Program mo�e by� napisany jako aplikacja
konsolowa lub mo�na skonstruowa� interfejs graficzny.
Do programu nale�y przygotowa� 3 pliki testowe do
wyboru (program powinien dzia�a� dla wszystkich instrukcji):
Zaj�cia MT cz1: 2,3,6,7,8,9,10,11,Zaj�cia MT cz2: 1,2,7

Schemat:
Opis: �negacja�
stany: 0,k
alfabet: 0,1,_
d�ugo�� s�owa: 6
s�owo: 011001
stan ko�cowy: k
stan pocz�tkowy: 0
instrukcja:
0:
1;0,1,r;
0;0,1,r;
_;k,_,s;

Opis instrukcji z przyk�adu wej�cia:
w stanie 0:
dla 1; po wykonaniu przejd� do stanu 0,zamie� na 1, przejd� w prawo;
dla 0; po wykonaniu przejd� do stanu 0,zamie� na 0, przejd� w prawo;
dla _; po wykonaniu przejd� do stanu k, zamie� na _, zatrzymaj;