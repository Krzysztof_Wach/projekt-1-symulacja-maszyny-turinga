class MaszynaTuringa:
    def __init__(self):
        self.opis = None
        self.stany = None
        self.alfabet = None
        self.slowo = None
        self.dlugoscSlowa = None
        self.stanKoncowy = None
        self.stanPoczatkowy = None
        self.wybranaPozycja = None
        self.instrukcja = {}


    def wczytaj(self, plik):  # pozwol klasie zdobyc wszystkie dane z instrukcji
        self.plik = plik
        self.dane = open(self.plik, 'r')
        for daneLine in self.dane:
            info = daneLine.split()
            self.ustalInformacje(info)

    def ustalInformacje(self, info):
        if len(info) > 0:
            if info[0] == 'Opis:':
                self.opis = info[1:]
            elif info[0] == 'stany:':
                self.stany = info[1].split(',')
            elif info[0] == 'alfabet:':
                self.alfabet = info[1].split(',')
            elif info[0] == 'słowo:':
                self.slowo = list(info[1])
                for znak in self.slowo:
                    if znak in self.alfabet:
                        pass
                    else:
                        raise TypeError('slowo zawiera symbol spoza alfabetu: ' + znak)
            elif info[0] == 'długość':
                self.dlugoscSlowa = info[2]
            elif info[0] == 'stan':
                if info[1] == 'końcowy:':
                    self.stanKoncowy = info[2]
                elif info[1] == 'początkowy:':
                    self.stanPoczatkowy = info[2]
            elif info[0] == 'wybrana':
                self.wybranaPozycja = info[2]
            elif info[0] == "instrukcja:":
                self.instrukcja = self.stworzInstrukcje()

    def stworzInstrukcje(self):
        instrukcja = {}
        for iloscStanow in range(len(self.stany)):
            if self.stany[iloscStanow] in self.stanKoncowy:
                instrukcja[self.stany[iloscStanow]] = {}
            else:
                linijka = self.dane.readline().rstrip().strip(':')
                stan = linijka[0]
                instrukcja[stan] = self.stworzInstrukcjeStanu(stan)
        return instrukcja

    def stworzInstrukcjeStanu(self, stan):
        instrukcjaStanu = {}
        for iloscZnakow in range(len(self.alfabet)): #dla stanow stworz znaki
            linijka = self.dane.readline().rstrip().strip('\t')
            znak = linijka[0]
            instrukcjaStanu[znak] = self.stworzInstrukcjeZnaku(linijka)
        return instrukcjaStanu

    def stworzInstrukcjeZnaku(self, linijka):
        instrukcjaZnaku = {}
        linijkaZnaku = []
        for element in linijka:
            if element != ';' and element != ',':
                linijkaZnaku.append(element)
        instrukcjaZnaku['nowyStan'] = linijkaZnaku[1]
        instrukcjaZnaku['nowyZnak'] = linijkaZnaku[2]
        instrukcjaZnaku['nowaPozycja'] = linijkaZnaku[3]
        return instrukcjaZnaku

    def aktywuj(self):
        poczatkoweSlowo = self.slowo #do transformacji slowa
        poczatkowyStan = self.stanPoczatkowy #do przechodzenia miedzy stanami
        if self.wybranaPozycja == '-':
            for i in range(len(self.slowo)):
                if self.slowo[i] != '_':
                    poczatkowaPozycja = i
                    break
        else:
            poczatkowaPozycja = self.wybranaPozycja-1
        return self.operacja(poczatkoweSlowo, poczatkowyStan, poczatkowaPozycja)

    def operacja(self, poczatkoweSlowo, poczatkowyStan, poczatkowaPozycja):
        aktualneSlowo = poczatkoweSlowo
        aktualnyStan = poczatkowyStan
        aktualnaPozycja = poczatkowaPozycja
        aktualnyZnak = aktualneSlowo[aktualnaPozycja]
        numerOperacji = 0
        print('\t| stan:', aktualnyStan)
        print(''.join(aktualneSlowo))

        while aktualnyStan not in self.stanKoncowy:
            numerOperacji+=1
            if numerOperacji == 40:
                print('maszyna wykonała zbyt dużą ilość instrukcji. prawdopodobny problem: pozycja nr', aktualnaPozycja)
                raise BufferError
            aktualneSlowo[aktualnaPozycja] = self.instrukcja[aktualnyStan][aktualnyZnak]['nowyZnak']

            if self.instrukcja[aktualnyStan][aktualnyZnak]['nowaPozycja'] == 'r':
                aktualnaPozycja += 1
            elif self.instrukcja[aktualnyStan][aktualnyZnak]['nowaPozycja'] == 'l':
                aktualnaPozycja -= 1
            elif self.instrukcja[aktualnyStan][aktualnyZnak]['nowaPozycja'] == 's':
                pass
            else:
                print(self.instrukcja[aktualnyStan][aktualnyZnak]['nowaPozycja'])
                raise ValueError + "instrukcja niepoprawnie przesuwa glowice"


            aktualnyStan = self.instrukcja[aktualnyStan][aktualnyZnak]['nowyStan']
            aktualnyZnak = aktualneSlowo[aktualnaPozycja]
            # zlacz i wyswietl
            print('\t| stan:', aktualnyStan)
            print(''.join(aktualneSlowo))
        infoKoncowe = str('program zakończył działanie instrukcji ' + ' '.join(self.opis) + ' w stanie "' + aktualnyStan +'"')
        raport = open(self.plik, 'a')
        raport.write('\n\nraport wykonanego zadania:\n'+
                     'slowo poczatkowe:'+ ''.join(self.slowo)+ '\n'+
                     'nazwa instrukcji:'+ ' '.join(self.opis)+ '\n'+
                     'wynik koncowy:'+ infoKoncowe)
        return infoKoncowe

if __name__ == "__main__":
    maszyna = MaszynaTuringa()
    print('podaj sciezke pliku (z .txt)')
    sciezka = str(input())
    try:
        maszyna.wczytaj(sciezka)
        print(maszyna.aktywuj())
    except FileNotFoundError:
        print('błąd w ścieżce pliku')
        raise


